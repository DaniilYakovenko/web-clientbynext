import React from 'react';
import { withRouter } from 'next/router';
import Layout from '../layouts/main';
import Head from 'next/head';
import Table from '../components/table';

const page = (props) => {
    return (
        <Layout>
            <Head>
                <title>Results</title>
            </Head>
            <Table text={'Results'}/>
        </Layout>
    )
};

export default withRouter(page);