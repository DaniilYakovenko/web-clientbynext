import React from 'react';
import { withRouter } from 'next/router';
import Layout from '../layouts/main';
import Head from 'next/head';
import axios from 'axios';
import Table from '../components/table';

const page = ({ data }) => {
    console.log(data);
    /*
        columns: [{
            data: string | number | function(row, type) // 'user.id' - lodash.property
            title: string
            class: string | function (data, type, row, meta) // meta is column type {data, title, class, render},
            render: string | function (data, type, row, meta) // meta is column type {data, title, class, render},
        },...]
    */
    return (
        <Layout>
            <Head>
                <title>Main</title>
            </Head>
            <Table data={data} columns={0} className="foo jh"></Table>
        </Layout>
    )
};

const url = 'http://10.11.40.189:8080/tvc-br-demo-2/patients';

page.getInitialProps = async ({ req, ...props }) => {
    const res = await axios.get(url);
    return { data: res.data };
}

export default withRouter(page);