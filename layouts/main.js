import React from 'react';
import { withRouter } from 'next/router';
import Link from '../components/activeLink';
import Head from 'next/head';
import 'bootstrap/dist/css/bootstrap.css';

const layout = ({ children, router }) => {
    return <div className="container">
        <Head>
            <meta charSet="utf-8" />
            <link rel="stylesheet" src="/static/../node_modules/bootstrap/dist/css/bootstrap.css"></link>
        </Head>
        <h1>TVC demo application</h1>
        <ul className="nav nav-tabs mb-3">
            <li className="nav-item">
                <Link activeClassName="active" href="/"><a className="nav-link">Patients</a></Link>
            </li>
            <li className="nav-item">
                <Link activeClassName="active" href="/results"><a className="nav-link ">Results</a></Link>
            </li>
        </ul>
        <div className="tab-content">
            <hr />
            {children}
            <hr />
        </div>
    </div>
}

export default withRouter(layout);