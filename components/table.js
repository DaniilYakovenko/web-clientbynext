import React from 'react';
import $ from 'jquery';

class Table extends React.Component {
    constructor(props) {
        super();
        this.data = props.data;
        this.columns = props.columns;
        this.className = props.className || "";
    }
    findCell(table, x, y) {

    }
    setUpCell(cell, settings) {
        
    }
    _createElement(tagName, children) {
        var element = document.createElement(tagName);
        if (!!children) {
            this._appendChildren(element, children)
        }
        return element;
    }
    createTable(header, body) {
        var thead = this._createElement('thead');
        var tbody = this._createElement('tbody');
        if (Array.isArray(header)) {
            this._appendChildren(thead, header.map(tr => this._createElement('tr', tr.map(th => this._createElement('th', th)))));
        };
        if (Array.isArray(body)) {
            this._appendChildren(tbody, body.map(tr => this._createElement('tr', tr.map(td => this._createElement('td', td)))));
        };
        return this._createElement('table', [thead, tbody]);
    }
    _isNode(node) {
        return node instanceof Node;
    }
    _asNode(data) {
        return this._isNode(data) ? data : document.createTextNode(data);
    }
    _appendChildren(target, children) {
        if (Array.isArray(children)) {
            for (var i = 0; i < children.length; i++) {
                children[i] && target.appendChild(this._asNode(children[i]));
            }
        } else {
            children && target.appendChild(this._asNode(children));
        }

        return target;
    }
    render() {
        return this.createTable([[1]], [[3]])
    }
}

export default Table;